import unittest

from flask_testing import TestCase

from app import create_app, db

import os

from flask import abort, url_for

from app.models import Group, User, Role
    
class BaseTestCase(TestCase):
    
    def create_app(self):

        # pass in test configurations
        config_name = 'testing'
        instance = False
        app = create_app(config_name, instance)
        app.config.update(
            SQLALCHEMY_DATABASE_URI='sqlite:////test.db',
            WTF_CSRF_ENABLED = False,
            CSRF_ENABLED = False,
            DEBUG = True,
            HASH_ROUNDS = 1,
            SECRET_KEY = "dfgipadipvqipdrt8qe8fasd8fgaslrfgbqwe"
        )
        return app
    

            
    def setUp(self):
        """
        Will be called before every test
        """

        db.create_all()

        # create test admin user
        admin = User(email="admin@local.host", username="admin", password="admin2020", is_admin=True)

        # create test non-admin user
        user = User(email="test_user@local.host", username="test_user", password="test2020")

        # save users to database
        db.session.add(admin)
        db.session.add(user)
        db.session.commit()
    
    def register(self, email, username, password, confirm, is_admin=False):
        return self.client.post(url_for('auth.register'),
        data=dict(email=email, username=username, password=password, confirm=confirm, is_admin=is_admin),
        follow_redirects=True)
 
    def login(self, email, password):
        return self.app.post(
            '/login',
            data=dict(email=email, password=password),
            follow_redirects=True
        )
    
    def logout(self):
        return self.app.get(
            '/logout',
            follow_redirects=True
        )
    
    
    def tearDown(self):
        """
        Will be called after every test
        """

        db.session.remove()
        db.drop_all()


class TestModels(BaseTestCase):
    
    def test_user_model(self):
        """
        Test number of records in User table
        """
        user = User(email="form.email.data",
                            username="form.username.data",
                            first_name="form.first_name.data",
                            last_name="form.last_name.data",
                            password="form.password.data")
        self.assertEqual(User.query.count(), 2)
        self.assertEqual(repr(user), "<User: {}>".format("form.username.data"))
        self.assertRaises(AttributeError,  getattr,user, "password")
        
    def test_group_model(self):
        """
        Test number of records in Group table
        """

        # create test group
        group = Group(name="IT", description="The IT Group")
        front = Group(name="Front", description="The Front Group")
        back = Group(name="Back", description="The Back Group")
        support = Group(name="Support", description="The Support Group")
        # save group to database
        db.session.add(group)
        db.session.add(front)
        db.session.add(back)
        db.session.add(support)
        db.session.commit()

        self.assertEqual(Group.query.count(), 4)
        self.assertEqual(repr(group), "<Group: {}>".format("IT"))
        
    def test_role_model(self):
        """
        Test number of records in Role table
        """

        # create test role
        role = Role(name="CEO", description="Run the whole company")
        qa = Role(name="QA", description="Test manager")
        developer = Role(name="Developer", description="Developer")
        po = Role(name="PO", description="Product owner")
        # save role to database
        db.session.add(role)
        db.session.add(qa)
        db.session.add(developer)
        db.session.add(po)
        db.session.commit()

        self.assertEqual(Role.query.count(), 4)
        self.assertEqual(repr(role), "<Role: {}>".format("CEO"))

class TestViews(BaseTestCase):

    def test_homepage_view(self):
        """
        Test that homepage is accessible without login
        """
        response = self.client.get(url_for('home.homepage'))
        self.assertEqual(response.status_code, 200)
        
        
    def test_login_view(self):
        """
        Test that login page is accessible without login
        """
        response = self.client.get(url_for('auth.login'))
        self.assertEqual(response.status_code, 200)

    def test_logout_view(self):
        """
        Test that logout link is inaccessible without login
        and redirects to login page then to logout
        """
        target_url = url_for('auth.logout')
        redirect_url = url_for('auth.login', next=target_url)
        response = self.client.get(target_url)
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, redirect_url)

    def test_dashboard_view(self):
        """
        Test that dashboard is inaccessible without login
        and redirects to login page then to dashboard
        """
        target_url = url_for('home.dashboard')
        redirect_url = url_for('auth.login', next=target_url)
        response = self.client.get(target_url)
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, redirect_url)
        

    def test_admin_dashboard_view(self):
        """
        Test that dashboard is inaccessible without login
        and redirects to login page then to dashboard
        """
        target_url = url_for('home.admin_dashboard')
        redirect_url = url_for('auth.login', next=target_url)
        response = self.client.get(target_url)
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, redirect_url)

    def test_groups_view(self):
        from flask_login import current_user
        """
        Test that groups page is inaccessible without login
        and redirects to login page then to groups page
        """
        target_url = url_for('admin.list_groups')
        redirect_url = url_for('auth.login', next=target_url)
        response = self.client.get(target_url)
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, redirect_url)

    # def test_groups_view_not_admin(self):
    #     from flask_login import current_user
    #     """
    #     Test that groups page 
    #     """

    #     response = self.client.post(url_for('auth.login'),
    #                             data={'email': 'user@local.host', 'password': 'user_not_register2020'})
    #     self.assert200(response)
    #     self.assertFalse(current_user.is_authenticated)
    #     self.assertFalse(current_user.is_active )
    #     redirect_403 = self.client.get(url_for('admin.list_groups'))
    #     self.assert403(redirect_403)
            
    def test_roles_view(self):
        """
        Test that roles page is inaccessible without login
        and redirects to login page then to roles page
        """
        target_url = url_for('admin.list_roles')
        redirect_url = url_for('auth.login', next=target_url)
        response = self.client.get(target_url)
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, redirect_url)

    def test_users_view(self):
        """
        Test that users page is inaccessible without login
        and redirects to login page then to users page
        """
        target_url = url_for('admin.list_users')
        redirect_url = url_for('auth.login', next=target_url)
        response = self.client.get(target_url)
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, redirect_url)



class TestLogin(BaseTestCase):
    
    def test_not_register_user(self):
        from flask_login import current_user
        with self.client:
            response = self.client.post(url_for('auth.login'),
                                    data={'email': 'user@local.host', 'password': 'user_not_register2020'})
            self.assert200(response)
            self.assertFalse(current_user.is_authenticated)
            self.assertFalse(current_user.is_active )
            self.assertTrue(current_user.is_anonymous)
            expected_flash_message = 'Invalid email or password.'
            self.assertMessageFlashed(expected_flash_message)

            
    def test_authenticated_admin_user(self):
        from flask_login import current_user
        with self.client:
            response = self.client.post(url_for('auth.login'),
                                    data={'email': 'admin@local.host', 'password': 'admin2020'})
            self.assert_redirects(response, url_for('home.admin_dashboard'))
            self.assertTrue(current_user.is_authenticated)
            self.assertTrue(current_user.is_active )
            self.assertTrue(current_user.is_admin )
            self.assertTrue(current_user.username, "admin")
            self.assertFalse(current_user.is_anonymous)
            
            
    def test_authenticated_not_admin_user(self):
        from flask_login import current_user
        with self.client:
            response = self.client.post(url_for('auth.login'),
                                    data={'email': 'test_user@local.host', 'password': 'test2020'})
            self.assert_redirects(response, url_for('home.dashboard'))
            self.assertTrue(current_user.is_authenticated)
            self.assertTrue(current_user.is_active )
            self.assertTrue(current_user.username, "test_user")
            self.assertFalse(current_user.is_admin )
            self.assertFalse(current_user.is_anonymous)

class TestLogout(BaseTestCase):
    
    def test_logout_not_admin_user(self):
        from flask_login import current_user
        with self.client:
            expected_flash_message = 'You have successfully been logged out.'
            response_login = self.client.post(url_for('auth.login'), 
                                              data={'email': 'test_user@local.host', 'password': 'test2020'})
            self.assert_redirects(response_login, url_for('home.dashboard'))
            self.assertTrue(current_user.is_authenticated)
            self.assertTrue(current_user.is_active )
            url = url_for('auth.logout')
            response = self.client.get(url)
            self.assertMessageFlashed(expected_flash_message, 'info')
            self.assert_redirects(response, url_for('auth.login'))

class TestRender(BaseTestCase):
       
    
    def test_render_homepage(self):
        response = self.client.get("/")
        self.assert_template_used('home/index.html')
        
    def test_render_register(self):
        response = self.client.get("/register")
        self.assert_template_used('auth/register.html')
        
    def test_render_home_dashboard(self):
        from flask_login import current_user
        with self.client:
            response = self.client.post(url_for('auth.login'),
                                    data={'email': 'test_user@local.host', 'password': 'test2020'})
            response = self.client.get("/dashboard")
            self.assert_template_used('home/dashboard.html')
            
    def test_render_home_admin_dashboard(self):
        from flask_login import current_user
        with self.client:
            response = self.client.post(url_for('auth.login'),
                                    data={'email': 'admin@local.host', 'password': 'admin2020'})
            response = self.client.get("/admin/dashboard")
            self.assert_template_used('home/admin_dashboard.html')
            
    def test_render_home_admin_dashboard_403(self):
        from flask_login import current_user
        with self.client:
            response = self.client.post(url_for('auth.login'),
                                    data={'email': 'test_user@local.host', 'password': 'test2020'})
            self.assert403(self.client.get("/admin/dashboard"))
            self.assert403(self.client.get("/admin/groups"))
            self.assert403(self.client.get("/admin/groups/add"))
            self.assert403(self.client.get("/admin/groups/edit/1"))
            self.assert403(self.client.get("/admin/groups/delete/1"))
            self.assert403(self.client.get("/admin/roles"))
            self.assert403(self.client.get("/admin/roles/add"))
            self.assert403(self.client.get("/admin/roles/edit/1"))
            self.assert403(self.client.get("/admin/roles/delete/1"))
            self.assert403(self.client.get("/admin/users"))
            self.assert403(self.client.get("/admin/users/assign/1"))
        with self.client:
            response = self.client.post(url_for('auth.login'),
                                    data={'email': 'admin@local.host', 'password': 'admin2020'})
            response = self.client.get("/admin/groups")
            self.assert_template_used('admin/groups/groups.html')
            response = self.client.get("/admin/groups/add")
            self.assert_template_used('admin/groups/group.html')
            #response = self.client.get(url_for('admin.edit_group', id="1"))
            #self.assert_template_used('admin/groups/group.html')
            response = self.client.get("/admin/roles")
            self.assert_template_used('admin/roles/roles.html')
            response = self.client.get("/admin/roles/add")
            self.assert_template_used('admin/roles/role.html')
            #response = self.client.get(url_for('admin.edit_role', id="1"))
            #self.assert_template_used('admin/roles/role.html')
            response = self.client.get("/admin/users")
            self.assert_template_used('admin/users/users.html')
            #response = self.client.get("/admin/users/assign/1")
            #self.assert_template_used()
            #response = self.client.get("/admin/groups/delete/1")
            #self.assert_template_used()
            #response = self.client.get("/admin/roles/delete/1")
            #self.assert_template_used()
        
class TestRegister(BaseTestCase):
   
        
    def test_register_user(self):
        with self.client:
            expected_flash_message = 'You have successfully registered! You may now login.'
            response = self.client.post(url_for('auth.register'),
                             data={
                                'email': 'form@email.data', 
                                'username': 'form.username.data', 
                                'first_name': 'form.first_name.data', 
                                'last_name': 'form.last_name.data', 
                                'password': 'formpassword', 
                                'confirm_password':'formpassword'})
            self.assert_redirects(response, url_for('auth.login'))
            self.assertMessageFlashed(expected_flash_message, 'info')
        
        
    def test_register_user_validate_exist_email_error(self):
        from app.auth.forms import RegistrationForm
        from wtforms import ValidationError
        
        f = RegistrationForm(email='test_user@local.host', 
                            username='form.username.data')
        with self.assertRaises(ValidationError) as ctx:
            f.validate_email(f['email'])
        self.assertIn("Email is already in use.", str(ctx.exception))
    def test_register_user_validate_exist_username_error(self):
        from app.auth.forms import RegistrationForm
        from wtforms import ValidationError
        
        f = RegistrationForm(email='form@email.data', 
                            username='admin')
        with self.assertRaises(ValidationError) as ctx:
            f.validate_username(f['username'])
        self.assertIn("Username is already in use.", str(ctx.exception))


class TestErrorPages(BaseTestCase):

    def test_403_forbidden(self):
        # create route to abort the request with the 403 Error
        @self.app.route('/403')
        def forbidden_error():
            abort(403)

        response = self.client.get('/403')
        
        
        
        print(response.data)
        self.assertTrue(b"403 Error" in response.data)

    def test_404_not_found(self):
        response = self.client.get('/nothinghere')
        self.assertEqual(response.status_code, 404)
        print(response.data)
        self.assertTrue(b"404 Error" in response.data)

    def test_500_internal_server_error(self):
        # create route to abort the request with the 500 Error
        @self.app.route('/500')
        def internal_server_error():
            abort(500)

        response = self.client.get('/500')
        self.assertEqual(response.status_code, 500)
        print(response.data)
        self.assertTrue(b"500 Error" in response.data)

if __name__ == '__main__':
    unittest.main()