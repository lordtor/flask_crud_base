# run.py

import os

from app import create_app

#config_name = os.getenv('FLASK_CONFIG')
config_name = 'development'
instance = True
app = create_app(config_name, instance)
#app = create_app()

if __name__ == '__main__':
    app.run()