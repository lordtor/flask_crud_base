# app/__init__.py

# third-party imports
from flask import abort, Flask, render_template
from flask_bootstrap import Bootstrap
from flask_login import LoginManager
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy
from flask_assets import Environment, Bundle
from flask_wtf.csrf import CSRFProtect

# local imports
from config import app_config

db = SQLAlchemy()
login_manager = LoginManager()
csrf = CSRFProtect()

#def create_app(config_name):
def create_app(config_name, instance):
    app = Flask(__name__, instance_relative_config=instance)
    csrf.init_app(app)
    app.config.from_object(app_config[config_name])
    app.config.from_pyfile('config.py')
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    Bootstrap(app)
    db.init_app(app)
    login_manager.init_app(app)
    login_manager.login_message = "You must be logged in to access this page."
    login_manager.login_view = "auth.login"
    migrate = Migrate(app, db)
    assets = Environment(app)
    js = Bundle(
    'js/jquery.min.js',
    'js/bootstrap.min.js',
    'js/popper.min.js',
    'js/bootstrap.bundle.min.js',
    'vendor/monaco-editor/min/vs/loader.js')
    #'vendor/monaco-editor/min/vs/editor/editor.main.css',
    css = Bundle(
    'css/style.css',
    'css/bootstrap.min.css',
    'css/all.css',
    'css/open-iconic-bootstrap.min.css',
    'css/sticky-footer-navbar.css'
    ) 
    

    with app.app_context():
        assets.register('js_all', js)
        assets.register('css_all', css)
    from app import models

    from .admin import admin as admin_blueprint
    app.register_blueprint(admin_blueprint, url_prefix='/admin')

    from .auth import auth as auth_blueprint
    app.register_blueprint(auth_blueprint)

    from .home import home as home_blueprint
    app.register_blueprint(home_blueprint)
    @app.errorhandler(403)
    def forbidden(error):
        return render_template('errors/403.html', title='Forbidden'), 403

    @app.errorhandler(404)
    def page_not_found(error):
        return render_template('errors/404.html', title='Page Not Found'), 404

    @app.errorhandler(500)
    def internal_server_error(error):
        return render_template('errors/500.html', title='Server Error'), 500
    @app.route('/500')
    def error():
        abort(500)
    return app